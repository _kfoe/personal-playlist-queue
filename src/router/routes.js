import Home from '../view/homepage/home.vue'

export default {
  routes: [{
    path: '/',
    name: 'home',
    component: Home
  }]
}