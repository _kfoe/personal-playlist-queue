import request from 'axios'
import _ from 'lodash'
import * as helpers from '../helpers'
export default {
  async getVideo({
    commit,
    state
  }, payload) {

    if (_.find(state.videoList, function(obj) {
        return obj.id == payload
      })) {
      commit('UPDATE_ALERT', 'video already exists')
      return false
    }

    try {
      var response = await request.get('https://content.googleapis.com/youtube/v3/videos?id=' + payload + '&maxResults=1&order=viewCount&part=snippet,contentDetails&type=video&videoEmbeddable=any&key=AIzaSyD7fOuSX-9p1_gexWLH7FcxbcX_FuI68hk&videoLiscense=any')
    } catch (e) {
      commit('UPDATE_ALERT', e.message)
      return false
    }

    if (response.data.items.length <= 0) {
      commit('UPDATE_ALERT', "Video not found.")
      return false
    }

    commit('ADD_VIDEO', response)

    helpers.set('VIDEO', JSON.stringify(state.videoList))

    if (1 === (state.videoList.length - 1) && 0 === state.latestsIndexVideoLists) {
      commit('NEXT_VIDEO')
      state.player.B.videoData = state.videoList[state.latestsIndexVideoLists]
    }

  }
}