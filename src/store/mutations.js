import _ from 'lodash'
import * as helpers from '../helpers'

export default {
  UPDATE_ALERT: (state, msg = false) => {
    if (msg === false) {
      state.alert = false
    } else {
      state.alert = true
      state.alertMessage = msg
    }
  },
  PUSH_ALREADY_EXISTS_VIDEOLIST: (state) => {
    state.videoList = JSON.parse(helpers.get('VIDEO'))
  },
  ADD_VIDEO: (state, response) => {
    state.videoList.push({
      title: response.data.items[0].snippet.title,
      id: response.data.items[0].id,
      duration: response.data.items[0].contentDetails.duration
    })
  },
  NEXT_VIDEO: (state) => {
    if (state.latestsIndexVideoLists >= (state.videoList.length - 1)) {
      state.latestsIndexVideoLists = 0
    } else {
      state.latestsIndexVideoLists += 1
    }
  },
  TACABANDA: (state, value) => {
    state.tacabanda = value
  },
  UPDATE_LAST_PLAYER: (state, newPlayer) => {
    state.lastPlayer = newPlayer
  },
  UPDATE_LAST_PLAYER_INIT: (state, newValue) => {
    state.latestPlayerInit = newValue
  },
  RESET_LAST_INDEX: (state) => {
    state.latestsIndexVideoLists = 0
  },
  RESET: (state) => {
    state.player = {
      A: {
        name: "A",
        instance: null,
        videoData: null
      },
      B: {
        name: "B",
        instance: null,
        videoData: null
      }
    };
    state.lastPlayer = null
    state.latestPlayerInit = false
  }
}