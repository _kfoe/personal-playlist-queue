import Vue from 'vue'
import Vuex from 'vuex'

import * as getters from './getters'
import actions from './actions'
import mutations from './mutations'

Vue.use(Vuex)

const defaultState = {
  videoList: [],
  latestsIndexVideoLists: -1,
  alert: false,
  alertMessage: "",
  player: {
    A: {
      name: "A",
      instance: null,
      videoData: null
    },
    B: {
      name: "B",
      instance: null,
      videoData: null
    }
  },
  lastPlayer: null,
  latestPlayerInit: false,
  tacabanda: false
}

const inBrowser = typeof window !== 'undefined'

const state = (inBrowser && window.__INITIAL_STATE__) || defaultState

export default new Vuex.Store({
  state,
  mutations,
  getters,
  actions
})