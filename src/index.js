import Vue from 'vue'
import base from './App.vue'

import './assets/scss/style.scss'

import router from './router/router'
import store from './store'
import Components from './components'
import filter from './filter'
import directive from './directive'
import {
  sync
} from 'vuex-router-sync'
import VueYoutubeEmbed from 'vue-youtube-embed'

sync(store, router)

Vue.use(VueYoutubeEmbed)

var instance = new Vue({
  router,
  store,
  el: '#app',
  render: h => h(base)
})

export default instance