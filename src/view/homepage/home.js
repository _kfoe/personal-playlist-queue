import {
  mapState,
  mapActions
} from 'vuex'
import * as helpers from '../../helpers'
import utils from './utils'
import _ from 'lodash'
export default {
  data: function() {
    return {
      query: '',
      preload: false,
      ops: {
        fadeInOutEffect: true,
        secondsUntilStart: 20,
        removeFromLocal: true
      }
    }
  },
  mounted: function() {
    var s = 0

    if (helpers.get('VIDEO') !== null && JSON.parse(helpers.get('VIDEO')).length > 0) {
      this.preload = true
      this.$store.commit('PUSH_ALREADY_EXISTS_VIDEOLIST')
      s = 7000
    }

    setInterval(() => {

      if (this.videoList.length >= 1 && !this.latestPlayerInit) {
        this.tacabandaInit()
        this.preload = false
      }

      if (this.latestPlayerInit) {

        var durationCurrentVideo = utils.calculateDuration(this.lastPlayer.videoData.duration)
        var currentTimerVideo = utils.seconds2time(this.lastPlayer.instance.getCurrentTime())

        var oreMinutiP = durationCurrentVideo.toString().split(':')
        oreMinutiP = oreMinutiP[0] + ":" + oreMinutiP[1]

        var oreMinutiD = currentTimerVideo.toString().split(':')
        oreMinutiD = oreMinutiD[0] + ":" + oreMinutiD[1]

        if (oreMinutiP === oreMinutiD) {
          var secondiRimanenti = utils.getDifferenceSecs(durationCurrentVideo, currentTimerVideo)
          if (secondiRimanenti == ((this.ops.secondsUntilStart > 60) ? 26 : this.ops.secondsUntilStart) || secondiRimanenti < 10) {
            console.log(secondiRimanenti)
            this.$store.commit('UPDATE_LAST_PLAYER', (this.lastPlayer.name == "A") ? this.player.B : this.player.A)
            this.lastPlayer.instance.playVideo();
            if (this.ops.fadeInOutEffect) {
              this.fadeInOutMusic(2)
            }
          }
        }
      }
    }, s)
  },
  computed: {
    parseVideoId: function() {
      var tQuery = this.query.match(/^.*(youtu.be\/|v\/|embed\/|watch\?|youtube.com\/user\/[^#]*#([^\/]*?\/)*)\??v?=?([^#\&\?]*).*/)
      tQuery = (Array.isArray(tQuery)) ? tQuery[3] : this.query || this.query
      return tQuery
    },
    ...mapState(['alert', 'videoList', 'alertMessage', 'tacabanda', 'player', 'latestsIndexVideoLists', 'lastPlayer', 'latestPlayerInit'])
  },
  methods: {
    tacabandaInit: function() {
      this.$store.commit('NEXT_VIDEO')
      this.player.A.videoData = this.videoList[this.latestsIndexVideoLists]
      this.$store.commit('NEXT_VIDEO')
      this.player.B.videoData = this.videoList[this.latestsIndexVideoLists]
      this.$store.commit('TACABANDA', true)
    },
    readyPlayer1: function(event) {
      this.player.A.instance = event.target
      this.$store.commit('UPDATE_LAST_PLAYER', this.player.A)
      setTimeout(() => {
        this.player.A.instance.playVideo()
      }, 1000)
      this.$store.commit('UPDATE_LAST_PLAYER_INIT', true)
    },
    readyPlayer2: function(event) {
      this.player.B.instance = event.target
    },
    endVideoPlayer1: function() {
      this.$store.commit('NEXT_VIDEO')
      this.player.A.videoData = this.videoList[this.latestsIndexVideoLists]
    },
    endVideoPlayer2: function() {
      this.$store.commit('NEXT_VIDEO')
      this.player.B.videoData = this.videoList[this.latestsIndexVideoLists]
    },
    eraseVideo: function(data) {

      if (this.videoList.length <= 1) {
        this.$store.commit('RESET_LAST_INDEX')
      }

      this.videoList.splice(data.index, 1)

      if (this.ops.removeFromLocal === true) {
        helpers.set("VIDEO", JSON.stringify(this.videoList))
      }

      if (this.videoList.length <= 0) {
        this.lastPlayer.instance.stopVideo()
        this.$store.commit('RESET')
        return false;
      }

      if (this.lastPlayer.videoData.id === data.video.id) {
        this.goToNextVideo()
      }

      if (2 === (this.videoList.length + 1) && 1 === this.latestsIndexVideoLists) {
        this.$store.commit('RESET_LAST_INDEX')
        this.player.B.videoData = this.videoList[this.latestsIndexVideoLists]
      }
    },
    goToNextVideo: function() {
      if (this.lastPlayer.name == "A") {
        this.lastPlayer.videoData = this.player.B.videoData
        this.$store.commit('NEXT_VIDEO')
        this.player.B.videoData = this.videoList[this.latestsIndexVideoLists]
      } else {
        this.lastPlayer.videoData = this.player.A.videoData
        this.$store.commit('NEXT_VIDEO')
        this.player.A.videoData = this.videoList[this.latestsIndexVideoLists]
      }

      setTimeout(() => {
        this.lastPlayer.instance.playVideo()
      }, 500)
    },
    fadeInOutMusic: function(secs) {
      var prevPlayer = (this.lastPlayer.name == "A") ? this.player.B : this.player.A
      var end = 100
      var start = 10
      setInterval(() => {

        if (start < 100) {
          this.lastPlayer.instance.setVolume(start += 10)
          console.log('volume player corrente ', this.lastPlayer.instance.getVolume())
        }

        if (end <= 100 && end > 0) {
          prevPlayer.instance.setVolume(end -= 10)
          console.log('volume player in chiusura ', prevPlayer.instance.getVolume())
        }

      }, (secs * 1000));
    },
    ...mapActions(['getVideo'])
  },
  watch: {
    query: function(query) {
      this.$store.commit('UPDATE_ALERT')
    }
  }
}