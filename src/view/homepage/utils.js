export default {
  calculateDuration: (duration, secs = false) => {
    var a = duration.match(/\d+/g);

    if (duration.indexOf('M') >= 0 && duration.indexOf('H') == -1 && duration.indexOf('S') == -1) {
      a = [0, a[0], 0];
    }

    if (duration.indexOf('H') >= 0 && duration.indexOf('M') == -1) {
      a = [a[0], 0, a[1]];
    }
    if (duration.indexOf('H') >= 0 && duration.indexOf('M') == -1 && duration.indexOf('S') == -1) {
      a = [a[0], 0, 0];
    }

    duration = 0;

    if (a.length == 3) {
      duration = duration + parseInt(a[0]) * 3600;
      duration = duration + parseInt(a[1]) * 60;
      duration = duration + parseInt(a[2])
    }

    if (a.length == 2) {
      duration = duration + parseInt(a[0]) * 60;
      duration = duration + parseInt(a[1])
    }

    if (a.length == 1) {
      duration = duration + parseInt(a[0])
    }

    var h = Math.floor(duration / 3600);
    var m = Math.floor(duration % 3600 / 60);
    var s = Math.floor(duration % 3600 % 60);

    if (secs) {
      return parseInt(((s.toString().length == 1) ? "0" + s : s))
    }

    return (
      ((h.toString().length == 1) ? "0" + h : h) + ":" + ((m.toString().length == 1) ? "0" + m : m) + ":" + ((s.toString().length == 1) ? "0" + s : s)
    )

  },
  seconds2time: function(seconds) {
    var hours = Math.floor(seconds / 3600);
    var minutes = Math.floor((seconds - (hours * 3600)) / 60);
    var seconds = (seconds - (hours * 3600) - (minutes * 60)).toFixed(0);

    return (
      ((hours.toString().length == 1) ? "0" + hours : hours) + ":" + ((minutes.toString().length == 1) ? "0" + minutes : minutes) + ":" + ((seconds.toString().length == 1) ? "0" + seconds : seconds)
    )
  },
  getDifferenceSecs: (currentTimerVideo, durationCurrentVideo) => {
    var a = ("2017-05-02T" + currentTimerVideo);
    var b = ("2017-05-02T" + durationCurrentVideo);

    var milliseconds = ((new Date(a)) - (new Date(b)));
    var minutes = Math.floor(milliseconds / 60000);
    var seconds = ((milliseconds % 60000) / 1000).toFixed(0);

    return seconds
  }
}