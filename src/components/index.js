import Vue from 'vue'

import header from './header/header.vue'
import footer from './footer/footer.vue'


Vue.component('app-header', {
  render: h => h(header)
})

Vue.component('app-footer', {
  render: h => h(footer)
})