import _ from 'lodash'

const set = (key, data) => {
  localStorage.setItem(key, data)
}

const get = (key) => {
  return localStorage.getItem(key)
}

const redirect = (vueinstance, route) => {
  vueinstance.$router.push(route)
}

export {
  alert,
  set,
  get,
  redirect,
  is_allow_type
}