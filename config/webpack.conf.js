const _path = require('path');
const appConfig = require('../appConfig')
module.exports = {
  optimization: {
    splitChunks: {
      cacheGroups: {
        default: false,
        vendors: false,
        vendor: {
          name: 'vendor',
          chunks: 'all',
          test: /node_modules/,
          priority: 20
        },
        common: {
          name: 'common',
          minChunks: 2,
          chunks: 'async',
          priority: 10,
          reuseExistingChunk: true,
          enforce: true
        }
      }
    }
  },
  common: appConfig.common,
  build: {
    mode: "production",
    devtool: false,
    output: {
      path: _path.resolve(__dirname, "./../build/prod"),
      filename: "assets/js/[name].[hash:8].js",
      publicPath: appConfig.domain
    }
  },
  dev: {
    mode: "development",
    devtool: "cheap-module-eval-source-map",
    output: {
      path: _path.resolve(__dirname, "./../build/dev"),
      filename: "assets/js/[name].[hash:8].js",
    },
    devServer: {
      historyApiFallback: true,
      noInfo: false,
      inline: true,
      hot: true,
      port: 8080
    }
  }
}