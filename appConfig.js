module.exports = {
  common: {
    isResponse: false,
    appTitle: "Personal Playlist Queue",
    appMountId: "app",
    seoDesc: ""
  },
  domain: "localhost:7000"
}