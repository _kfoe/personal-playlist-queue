const _path = require('path');
const baseWebpackConfig = require('./webpack.base')


const webpack = require('webpack');
const clean = require('clean-webpack-plugin');
const copy = require('copy-webpack-plugin');
const html = require('html-webpack-plugin');
var merge = require('webpack-merge');
const VueLoader = require('vue-loader')
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const FriendlyErrorsPlugin = require('friendly-errors-webpack-plugin');
const postcssPresetEnv = require('postcss-preset-env');

const config = require('../config/webpack.conf')

const C = (process.env.NODE_ENV === "production") ? config.build : config.dev

module.exports = merge(baseWebpackConfig, {
  resolve: {
    alias: {
      vue: "vue/dist/vue.js",
    }
  },
  plugins: [
    new clean(['build/dev/css', 'build/dev/js', 'build/dev/fonts'], {
      root: _path.resolve(__dirname, '../'),
      verbose: true,
      dry: false,
      exclude: []
    }),
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NoEmitOnErrorsPlugin(),
    new webpack.optimize.OccurrenceOrderPlugin(),
    new VueLoader.VueLoaderPlugin(),
    new MiniCssExtractPlugin({
      filename: "assets/css/[name].css",
    }),
    new html({
      inject: false,
      template: require('html-webpack-template'),
      title: config.common.appTitle,
      appMountId: config.common.appMountId,
      baseHref: (process.env.NODE_ENV === "production") ? config.build.output.publicPath : null,
      mobile: config.common.isResponse,
      meta: [{
        name: 'description',
        content: config.common.seoDesc
      }],
    }),
    new FriendlyErrorsPlugin()
  ]
})