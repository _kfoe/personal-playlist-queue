const _path = require('path');
const baseWebpackConfig = require('./webpack.base')
const webpack = require('webpack');
const clean = require('clean-webpack-plugin');
const html = require('html-webpack-plugin');
const config = require('../config/webpack.conf');
var merge = require('webpack-merge');
const VueLoader = require('vue-loader')
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const CompressionWebpackPlugin = require("compression-webpack-plugin")
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;
const C = (process.env.NODE_ENV === "production") ? config.build : config.dev

module.exports = merge(baseWebpackConfig, {
  resolve: {
    alias: {
      vue: "vue/dist/vue.min.js",
    }
  },
  plugins: [
    new clean(['build/prod'], {
      root: _path.resolve(__dirname, '../'),
      verbose: true,
      dry: false,
      exclude: []
    }),
    new webpack.HashedModuleIdsPlugin(),
    new VueLoader.VueLoaderPlugin(),
    new html({
      inject: false,
      template: require('html-webpack-template'),
      title: config.common.appTitle,
      appMountId: config.common.appMountId,
      baseHref: (process.env.NODE_ENV === "production") ? config.build.output.publicPath : null,
      mobile: config.common.isResponse,
      meta: [{
        name: 'description',
        content: config.common.seoDesc
      }],
      minify: {
        html5: true,
        removeComments: true,
        collapseWhitespace: true,
        removeAttributeQuotes: false,
      },
      // necessario per CommonsChunkPlugin
      chunksSortMode: 'dependency'
    }),
    new MiniCssExtractPlugin({
      filename: "assets/css/[name].css",
    }),
    new CompressionWebpackPlugin({
      asset: '[path].gz[query]',
      deleteOriginalAssets: true,
      algorithm: 'gzip',
      test: /\.js$|\.css$|\.html$|\.eot?.+$|\.ttf?.+$|\.woff?.+$|\.svg?.+$/,
      threshold: 10240,
      minRatio: 0.8
    }),
    new BundleAnalyzerPlugin()
  ]
})