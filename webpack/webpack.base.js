const config = require('../config/webpack.conf')
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const postcssPresetEnv = require('postcss-preset-env');
const cssnano = require('cssnano');
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;
const C = (process.env.NODE_ENV === "production") ? config.build : config.dev

module.exports = {
  mode: C.mode,
  devtool: C.devtool,
  entry: ['babel-polyfill', require('path').resolve(__dirname, '../src/index.js')],
  output: C.output,
  module: {
    rules: [{
      test: /\.vue$/,
      exclude: /node_modules/,
      use: [
        "vue-loader"
      ]
    }, {
      test: /\.js$/,
      exclude: /node_modules/,
      use: {
        loader: "babel-loader",
        options: {
          plugins: [
            //transform-runtime
            "transform-object-rest-spread",
            "transform-async-to-generator",
          ],
          presets: [
            ["env", {
              modules: false
            }]
          ]
        }
      }
    }, {
      test: /\.(css|sass|scss)$/,
      use: [
        'style-loader',
        MiniCssExtractPlugin.loader, {
          loader: 'css-loader',
          options: {
            importLoaders: 2,
            sourceMap: true
          }
        }, {
          loader: 'postcss-loader',
          options: {
            plugins: () => [
              postcssPresetEnv({
                browsers: ['last 4 versions']
              }),
              cssnano()
            ],
            sourceMap: true
          }
        }, {
          loader: 'sass-loader',
          options: {
            sourceMap: true
          }
        }
      ]
    }, {
      test: /\.(eot?.+|svg?.+|ttf?.+|otf?.+|woff?.+|woff2?.+)$/,
      use: 'file-loader?name=assets/[name]-[hash].[ext]'
    }, {
      test: /\.(png|gif|jpg|svg|jpeg)$/,
      use: [
        'url-loader?limit=20480&name=assets/[name]-[hash].[ext]'
      ]
    }]
  },
  optimization: {
    minimize: (process.env.NODE_ENV === "production") ? true : false,
    splitChunks: config.optimization.splitChunks
  }
}